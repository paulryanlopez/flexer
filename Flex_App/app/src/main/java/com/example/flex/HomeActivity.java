package com.example.flex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    private Button bmiButton;
    private Button callButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bmiButton = (Button) findViewById(R.id.bmiCalculator);
        bmiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBmiCalculator();
            }
        });

        callButton = (Button) findViewById(R.id.Trainer);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAssistant();
            }
        });
    }

    public void openAssistant(){
        Intent intentCall = new Intent(this, TrainerActivity.class);
        startActivity(intentCall);
    }

    public void openBmiCalculator(){
        Intent intent = new Intent(this, bmiCalculator.class);
        startActivity(intent);
    }
}